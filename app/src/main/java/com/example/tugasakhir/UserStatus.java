package com.example.tugasakhir;

public final class UserStatus {

    public static Status status = Status.NONE;

    private UserStatus(){}
}

enum Status{
    REGISTER, LOGIN, NONE
}