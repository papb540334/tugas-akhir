package com.example.tugasakhir;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.net.URI;


public class NewsClicked extends AppCompatActivity {
    ImageButton backBT, tripleDot;
    TextView deskripsi, judul;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_description);

        backBT = findViewById(R.id.btnBack);
        tripleDot = findViewById(R.id.buttonPopUp);
        deskripsi = findViewById(R.id.textIsi);
        judul = findViewById(R.id.textTitle);
        imageView = findViewById(R.id.newsIMG);

        if (getIntent() != null){
            Intent intent = getIntent();
            Berita berita = (Berita) intent.getSerializableExtra("berita");
            if(berita != null){
                judul.setText(berita.getJudul());
                deskripsi.setText(berita.getDeskripsi());
                Glide.with(getBaseContext()).load(berita.getImageURL()).into(imageView);
            }
        }

        backBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewsClicked.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        Context context = this;
        tripleDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context, v); // Use 'context' as the first argument
                popupMenu.getMenuInflater().inflate(R.menu.icon_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        final int menuEdit = R.id.menuEdit;
                        final int menuDelete = R.id.menuDelete;
                        if(item.getItemId() == menuEdit){
                            Intent intent = new Intent(context, Compose.class);
                            intent.putExtra("berita", getIntent().getSerializableExtra("berita"));
                            startActivity(intent);
                        } else if(item.getItemId() == menuDelete){
                            Toast.makeText(context, "Ini adalah menu delete", Toast.LENGTH_SHORT).show();
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference beritaRef = database.getReference("Berita");
        String clickedItemID = getIntent().getStringExtra("ID");
        beritaRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Handle errors that occurred while trying to read the data
                Log.e("Firebase", "Failed to read value.", error.toException());
            }
        });
    }
}
